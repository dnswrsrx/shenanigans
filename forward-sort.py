example_list = [4,3,3,2,1,5]

def swapper(nums):
    checker = True
    for i in range(len(nums)-1):
        if nums[i] > nums[i+1]:
            nums[i], nums[i+1] = nums[i+1], nums[i]
            checker = False
            i += 1
        else:
            i += 1
    return(nums, checker)

example_list, check = swapper(example_list)
while check == False:
    example_list, check = swapper(example_list)
    print(example_list)

