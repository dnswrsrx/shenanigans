# Convert Roman numerals from user into integers
# https://www.reddit.com/r/pythontips/comments/890dwe/problems_passing_variables_with_functions/

user_input = input("Enter a number in Roman numeral: ")

CONVERSION = {
        'M': 1000,
        'D': 500,
        'C': 100,
        'L': 50,
        'X': 10,
        'V': 5,
        'I': 1
        }

def convert_from(roman_numeral):
    ''' Converts each character into the integer equivalent and puts it in a
    list. If a number is smaller than the next, turn it into a negative
    '''
    converted_list = [CONVERSION[character] for character in
            roman_numeral.upper()]
    for i in range(len(converted_list)-1):
        if converted_list[i] < converted_list[i+1]:
            converted_list[i] = -converted_list[i]
    print(converted_list)
    return(converted_list)

def check_validity(converted_list):
    ''' Checks validity of the roman numeral input based on some rule:
    0) If first character is smaller than next, next shouldn't be smaller than
    one after
    1) next character must be within ten times of current
    2) a character and the next cannot be 5, 50, or 500
    3) next character cannot be double of previous
    4) if character and next are identical, previous cannot be smaller than
    character
    '''
    for i in range(len(converted_list)-1):
        if converted_list[i] < 0 and converted_list[i+1] < 0:
            return('error 0')
        elif abs(converted_list[i])*10 < abs(converted_list[i+1]):
            return('error 1')
        elif abs(converted_list[i]) in [5,50,500] and abs(converted_list[i+1]) in [5,50,500]:
            return('error 2')
        elif abs(converted_list[i])*2 == abs(converted_list[i+1]):
            return('error 3')
        elif (abs(converted_list[i]) == abs(converted_list[i+1]) and
                converted_list[i-1] < converted_list[i] and
                converted_list[i-1]< 0):
            return('error 4')
    else:
        return(sum(converted_list))

if __name__ == '__main__':
    print(check_validity(convert_from(user_input)))

