# Coding Challenge
# Reddit: https://www.reddit.com/r/learnprogramming/comments/847ckg/i_failed_a_coding_interview_but_then_afterwards/
# The problem was given a file of sentences, one sentence per line,
# implement a function that takes in 2 words, prob(a, b),
# and outputs the probability of a occurring given that the
# preceding word is b.

# Some test data already in a list with punctuation removed
sentences = ["Please buy me a drink",
"Please buy me a drink Bob",
"Please get me a drink Sue",
"Please find me a drink Joe",
"Please give me a Coke Joe"
]


all_words = [word.lower() for sentence in sentences for word in
             sentence.split(" ")]
'''
def prob(a, b):
    count = 0
    for i in range(len(all_words)):
        if all_words[i] == a and all_words[i-1] == b:
            count += 1
    return(count/len(all_words))
'''

def prob1(a, b):
    a = a.lower()
    b = b.lower()

    count = [1 for i in range(len(all_words)-1)
             if all_words[i] == b and all_words[i+1] == a]
    result = "Instances of '%s' after '%s': %s, total words: %s, probability: %s" %\
              (a, b, sum(count), len(all_words),
               sum(count)/len(all_words))
    return(result)

print(prob1('buy', 'please'))
#print(prob1('please', 'joe'))
