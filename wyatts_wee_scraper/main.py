import os

import utils

csv_name = "links_to_counsellors.csv"
results_file = "results.csv"
initial_url = "https://www.psychologytoday.com/ca/therapists/testing-and-evaluation/alberta?sid=5f0e9bae3c9e6&spec=184"

if not csv_name in os.listdir("./"):
    utils.get_links(initial_url, csv_name)

print('Links downloaded')
utils.get_details(csv_name, results_file=results_file)
