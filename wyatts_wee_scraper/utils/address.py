import csv
import urllib.error
import urllib.request

import bs4
from bs4 import BeautifulSoup

import utils


def normalise(value):
    if isinstance(value, bs4.element.Tag) and isinstance(value.string, bs4.element.NavigableString):
        return value.string.strip().replace(',', '')
    elif isinstance(value, bs4.element.NavigableString):
        return value.strip().replace(',', '')
    return value


def read_in_links(file_name):

    with open(file_name, 'r') as f:
        links = [row.strip() for row in f.readlines()]
    return links


def get_location(bs):
    street = normalise(bs.find(itemprop="streetAddress"))
    locality = normalise(bs.find(itemprop="addressLocality"))
    province = normalise(bs.find(itemprop="addressRegion"))
    postal_code = normalise(bs.find(itemprop="postalcode"))

    latitude, longitude = None, None

    if (lat := bs.find(itemprop="latitude")):
        latitude = lat.get("content")

    if (lon := bs.find(itemprop="longitude")):
        longitude = lon.get("content")

    return [
        street,
        locality,
        province,
        postal_code,
        latitude,
        longitude,
    ]


def get_cost(bs):
    cost = None
    for element in bs.select(".finances-office > ul > li"):
        contents = element.contents
        if "Cost per Session" in contents[-2].string:
            return contents[-1].string.strip()


def get_insurance(bs):
    insurance = []
    for element in bs.select(".attributes-insurance > div > ul > li"):
        insurance.append(normalise(element))
    return insurance if insurance else None


def get_details(file_name, results_file):

    links_and_addresses = []

    for i, link in enumerate(read_in_links(file_name)):

        try:
            print(i, link)
            with urllib.request.urlopen(utils.request(link)) as r:
                bs = BeautifulSoup(r.read(), "html.parser")

                name = normalise(bs.find(itemprop="name"))
                company = normalise(bs.select_one(".address-data").contents[0])
                cost = get_cost(bs)
                insurance = ','.join(ins) if (ins := get_insurance(bs)) else None

                location = get_location(bs)

                links_and_addresses.append(
                    [name, company, link, cost, insurance] + location
                )
        except urllib.error.HTTPError:
            links_and_addresses.append(None)


    with open(results_file, "w") as f:
        csv_file = csv.writer(f)
        for link_and_address in links_and_addresses:
            csv_file.writerow(link_and_address)

    return links_and_addresses
