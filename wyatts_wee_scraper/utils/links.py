import csv
import urllib.request

from bs4 import BeautifulSoup

import utils

def get_links(url, csv_name):
    links = []

    while True:

        with urllib.request.urlopen(utils.request(url)) as r:
            bs = BeautifulSoup(r.read(), "html.parser")

        for element in bs.select(".result-name"):
            links.append(element.get("href"))

        if (next_page := bs.select_one(".btn-next")):
            url = next_page.get("href")
        else:
            break

    with open(csv_name, "w") as f:
        csv_file = csv.writer(f)
        for link in links:
            csv_file.writerow([link])

    return links
