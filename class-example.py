class Car:
    def __init__(self, top_speed):
        self.top_speed = top_speed

    def mph_converter(self):
        return(self.top_speed * 2)

civic = Car(98) # Initialises an instance of class Car, instance being civic
print(civic.mph_converter()) # Runs mph_converter method
print(civic.top_speed) # Test to see if civic instance prints out input top_speed


class Car2:
    def __init__(self, top_speed):
        self.top_speed = top_speed

    def mph_converter(top_speed): # note the lack of self
        return(top_speed * 2)

    def up_mph_converter(cls): # Here, I tried assigning a class and look how its role is the same as self.
        return(cls.top_speed * 2)

accord = Car2(100)
#print(accord.mph_converter()) # Uncomment to run, but will print an error.
#print(accord.mph_converter(accord.top_speed)) #Uncomment to run, but will print an error.
print(accord.up_mph_converter())
print(Car2.mph_converter(accord.top_speed)) # see how to run the method, you would have to call the method through the class and not the instance, and assign the instance as an argument.
print(accord.top_speed)
    
