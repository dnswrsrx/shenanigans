# Convert Roman numerals from user into integers
# https://www.reddit.com/r/pythontips/comments/890dwe/problems_passing_variables_with_functions/

user_input = input("Enter a number in Roman numeral: ")

CONVERSION = {
        'M': 1000,
        'D': 500,
        'C': 100,
        'L': 50,
        'X': 10,
        'V': 5,
        'I': 1
        }

def convert_from(roman_numeral):
    ''' Converts each character into the integer equivalent and puts it in a
    list. If a number is smaller than the next, turn it into a negative
    '''
    converted_list = [CONVERSION[character] for character in
            roman_numeral.upper()]
    for i in range(len(converted_list)-1):
        if converted_list[i] < converted_list[i+1]:
            converted_list[i] = -converted_list[i]
    #print(converted_list)
    return(sum(converted_list))

if __name__ == '__main__':
    print(convert_from(user_input))

