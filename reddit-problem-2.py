'''
(https://www.reddit.com/r/AskProgramming/comments/864kfy/prolog_list_of_sublists_question/)
Given a list that contains n lists of size m each, make a list that contains n lists, where each of these sublists contain the ith element of all the previous lists. So, for example, if I have a starting list like this:

L = [ [a, b, c] , [d, e, f], [g, h, i] ]

I want to get this output:

R = [ [a, d, g], [b, e, h], [c, f, i] ]

Any ideas on how?
'''
# After writing out the problem in Python, realised it was to do with the
# Prolog programming langauge.

L = [['a','b','c'], ['d','e','f'], ['g','h','i']]

R = [[element[i] for element in L] for i in range(len(L))]
print(R)

