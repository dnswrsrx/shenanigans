sequence = {x: '' for x in range(1, 101)}

'''
fizz if divisible by 3
buzz if divisble by 5
fizzbuzz if divisble by both
'''

for x in sequence:
    if x % 3 == 0:
        sequence[x] += 'fizz'
    if x % 5 == 0:
        sequence[x] += 'buzz'
    if sequence[x] == '':
        sequence[x] += str(x)
'''
    if x % 15 == 0:
        sequence[x].append('15fizzbuzz')
'''
print(sequence)

