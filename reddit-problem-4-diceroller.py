from random import randint

# Roll dice and compile results
def info_of_all_lines(challenge_input):
    collection = [info_per_line(collect_role_results(line)) for line in
            challenge_input.split('\n') if line != '']
    return '\n'.join(collection)

def info_per_line(roll_results):
    rolls = ' '.join([str(roll) for roll in roll_results])
    return '{0}: {1}'.format(sum(roll_results), rolls)

def collect_role_results(challenge_input):
    rolls, max_dice_value = challenge_input.split('d')
    return [role_dice(max_dice_value) for roll in range(int(rolls))]

def role_dice(max_dice_value):
    return randint(1, int(max_dice_value))

# Check with user if they have die input
def more_rolls_from_user(challenge_input):
    while evaluate_user_input() == 'y':
        challenge_input = '\n'.join((challenge_input, get_input_from_user()))
    return challenge_input

def evaluate_user_input():
    user_input = input('More rolls? y/n: ').lower()
    if user_input not in ['y', 'n']:
        print('Invalid input. Please enter "y" or "n".')
        return(evaluate_user_input())
    else:
        return user_input

def get_input_from_user():
    def get_roll_number():
        roll = input('Number of rolls: ')
        if not roll.isdigit():
            print('Please enter an integer.')
            return get_roll_number()
        else:
            return roll

    def get_dice_type():
        dice = input('Enter dice type: ')
        if not dice.isdigit():
            print('Please enter an integer.')
            return get_dice_type()
        else:
            return dice

    return '{0}d{1}'.format(get_roll_number(), get_dice_type())


if __name__ == '__main__':
    with open('reddit-problem-4-input.txt', 'r') as challenge_input:
        challenge_input = challenge_input.read()
    challenge_input = more_rolls_from_user(challenge_input)
    info_output = info_of_all_lines(challenge_input)
    #print(challenge_input)
    print(info_output)
